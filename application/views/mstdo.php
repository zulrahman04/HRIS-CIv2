
<div class="card">
    <div class="card-header">
        <button class="btn btn-primary" onclick="formAdd()"><i class="nav-icon fa fa-fw fa-plus"></i> Tambah</button>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped"  id="mydata">
                <thead>
                    <tr>
                    <th width='5px'>No</th>
                    <th>Tanggal</th>
                    <th>Keterangan</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php $no = 1;
                        foreach ($do as $key) {
                            $date = date_format(date_create($key->tanggal),"d/m/Y") ?>
                    <tr>
                        <td><?= $no++?></td>
                        <td><?= $date?></td>
                        <td><?= $key->keterangan?></td>
                        <td>
                        <button class="btn btn-info btn-sm" onclick="formEditDo('<?= $key->tanggal ?>')"><i class="fa fa-fw fa-edit"></i></button>
                        <button class="btn btn-danger btn-sm" onclick="deleteDo(<?= $key->id ?>)"><i class="fa fa-fw fa-trash"></i></button>
                        </td>
                    </tr>
                        <?php } ?>
                </tbody>
            </table>
        </div>
    <!-- /.table-responsive -->
    </div>
    <!-- /.card-body -->
    <!-- /.card-footer -->
</div>
<!-- /.card --> 

<div class="modal fade" id="modal-add">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Form Tambah To</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="form-horizontal" id="frmNewDo">
          <div class="modal-body">
            <div class="form-body">
                <div class="form-group">
                    <label for="tgl">Tanggal
                        <span class="text-danger"> * </span>
                    </label>
                    <input type="date" id="tgl" name="tgl" class="form-control"/>
                  </select>
                </div>
                <div class="form-group">
                    <label for="ket">Keterangan
                        <span class="text-danger"> * </span>
                    </label>
                    <input type="text" id="ket" name="ket" class="form-control"/>
                </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
      <!-- /.modal-content -->
  </div>
</div>

<div class="modal fade" id="modal-edit">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit To</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="form-horizontal" id="frmEditDo">
          <div class="modal-body">
            <div class="form-body">
              <div class="form-group">
                  <label for="tgl2">Tanggal
                      <span class="text-danger"> * </span>
                  </label>
                  <input type="hidden" id="id" name="id" class="form-control"/>
                  <input type="date" id="tgl2" name="tgl2" class="form-control"/>
              </div>
              <div class="form-group">
                  <label for="ket2">Keterangan
                      <span class="text-danger"> * </span>
                  </label>
                  <input type="text" id="ket2" name="ket2" class="form-control"  />
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" >Save changes</button>
          </div>
        </form>
    </div>
      <!-- /.modal-content -->
  </div>
</div>

<script>
    $(document).ready(function () { 
        $('#frmNewDo').validate({
            rules: {
                    tgl: {
                    required: true
                },
                    ket: {
                    required: true
                }
            },
            messages: {
                    tgl: {
                    required: "Masukan Tanggal",
                },
                    ket: {
                    required: "Masukan Keterangan"
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },

            submitHandler: function () {
                $.ajax({
                dataType: "json",
                type: 'POST', 
                url: '<?= base_url() ?>mstdo/addDo',
                data: {
                    tgl: $('#tgl').val(),
                    ket: $('#ket').val()     
                },
                success: function(response) {
                    if (response.result == 'Berhasil') {
                    successtr(response.message)
                    }else{
                    errortr(response.message)
                    } 
                },
                error: function() {          
                    error()
                }
                });
            }
        });
        $('#frmEditDo').validate({
            rules: {
                    tgl2: {
                    required: true
                },
                    ket2: {
                    required: true
                }
            },
            messages: {
                    tgl2: {
                    required: "Masukan Tanggal",
                },
                    ket2: {
                    required: "Masukan Keterangan"
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            
            submitHandler: function () {
                $.ajax({
                    dataType: "json",
                    type: 'POST', 
                    url: '<?= base_url() ?>mstdo/saveEditDo',
                    data: {
                        id: $('#id').val(),
                        tgl: $('#tgl2').val(),
                        ket: $('#ket2').val(),
                    },
                    success: function(response) { 
                        if (response.result == 'Berhasil') {                 
                            successtr(response.message)
                        }else{
                            errortr(response.message)
                        } 
                    },
                    error: function() {          
                        error()
                    }
                });
            }
        });
    });

    function formAdd(){
        $('#tgl').val('')
        $('#ket').val('')
        $('#modal-add').modal()
    }

    function formEditDo(tgl){
        $('#tgl2').val('')
        $('#ket2').val('')
        $('#modal-edit').modal()
        $.ajax({
            dataType: "json",
            type: 'POST', 
            url: '<?= base_url() ?>mstdo/getDoEdit',
            data: {
                tgl: tgl
            },
            success: function(response) { 
                // console.log(tgl)
                $('#id').val(response.id)
                $('#tgl2').val(response.tanggal)
                $('#ket2').val(response.keterangan)
            },
            error: function() {
                error()
            }
        });
    }

    function Capital() {
        var x = document.getElementById("code");
        x.value = x.value.toUpperCase();

        var y = document.getElementById("code2");
        y.value = y.value.toUpperCase();        
    }

    function deleteTo(id){
        Swal.fire({
        title: 'Anda yakin?',
        text: "Anda tidak bisa mengembalikannya!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) { 
            $.ajax({
            dataType: "json",
            type: 'POST', 
            url: '<?= base_url() ?>mstTo/deleteTo',
            data: {
                id: id
            },
            success: function(response) { 
                if (response.result == 'Berhasil') { 
                successtr(response.message)
                }else{
                errortr(response.message)
                }
            },
            error: function() {  
                error()
            }
            });
        }
        })
    }

    $("#mydata").DataTable({
        "responsive": true,
        "autoWidth": false,
    });

    function deleteDo(id){
        Swal.fire({
        title: 'Anda yakin?',
        text: "Anda tidak bisa mengembalikannya!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) { 
                $.ajax({
                dataType: "json",
                type: 'POST', 
                url: '<?= base_url() ?>mstdo/deleteDo',
                data: {
                    id: id
                },
                success: function(response) { 
                    if (response.result == 'Berhasil') { 
                        successtr(response.message)
                    }else{
                        errortr(response.message)
                    }
                },
                error: function() {  
                    error()
                }
                });
            }
        })
    }
</script>