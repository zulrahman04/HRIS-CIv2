
    <div class="card">
      <div class="card-header">
        <button class="btn btn-primary" onclick="formRole()"><i class="nav-icon fa fa-fw fa-plus"></i> Tambah</button>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped"  id="mydata">
            <thead>
              <tr>
                <th width='5px'>No</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Jenis kelamin</th>
                <th>Departemen</th>
                <th>Seksi</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.card-body -->
      <!-- /.card-footer -->
    </div>
  <!-- /.card --> 

<script>
 
    $(document).ready(function(){

        table = $('#mydata').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?= base_url() ?>pegawai/listPegawai",
                "type": "POST"
            },
        });
    });
</script>