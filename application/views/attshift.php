<div class="col-md-12">
    <!-- general form elements disabled -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Generate Attendance Shift</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form role="form" id="formGenerate">
                <div class="col-12 row">
                    <div class="col-4">
                        <div class="card ">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Departemen</label>
                                            <select class="form-control select2" style="width: 100%;" id="dep" name="dep" >
                                                <option value="">-- PILIH DEPARTEMEN --</option>
                                                <?php foreach ($dept as $key) {?>
                                                    <option value="<?= $key->id_departemen ?>"><?= $key->nama_departemen ?></option>
                                                <?php } ?>                    
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Seksi</label>
                                            <select class="form-control select2" style="width: 100%;" id="seksi" name="seksi">
                                                <option value="">-- PILIH SEKSI --</option>                   
                                            </select>
                                        </div>
                                    </div>                            
                                </div>
                                <button type="button" class="btn btn-primary" id="vw">View</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="card ">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">   
                                            <label>Bulan</label>                                         
                                            <div class="input-group mb-3" id="reservationdate" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" id="date" name="date" placeholder="MM/YYYY"/>
                                                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Senin - Kamis</label>
                                            <select class="form-control select2" style="width: 100%;" id="opt1" name="opt1">
                                                <option value="">- PILIH SHIFT -</option>
                                                <?php foreach ($shift as $key) {?>
                                                    <option value="<?= $key->shift ?>"><?= $key->shift ?>  <?= $key->in ?>-<?= $key->out ?></option>
                                                <?php } ?>   
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Jumat</label>
                                            <select class="form-control select2" style="width: 100%;" id="opt2" name="opt2">
                                                <option value="">- PILIH SHIFT -</option>
                                                <?php foreach ($shift as $key) {?>
                                                    <option value="<?= $key->shift ?>"><?= $key->shift ?>  <?= $key->in ?>-<?= $key->out ?></option>
                                                <?php } ?>   
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Sabtu</label>
                                            <select class="form-control select2" style="width: 100%;" id="opt3" name="opt3">
                                                <option value="">- PILIH SHIFT -</option>
                                                <?php foreach ($shift as $key) {?>
                                                    <option value="<?= $key->shift ?>"><?= $key->shift ?>  <?= $key->in ?>-<?= $key->out ?></option>
                                                <?php } ?>   
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Minggu</label>
                                            <select class="form-control select2" style="width: 100%;" id="opt4" name="opt4">
                                                <option value="">- PILIH SHIFT -</option>
                                                <?php foreach ($shift as $key) {?>
                                                    <option value="<?= $key->shift ?>"><?= $key->shift ?>  <?= $key->in ?>-<?= $key->out ?></option>
                                                <?php } ?>   
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-12" id="view">
    <div class="card" >
        <div class="card-body">            
            <div class="table-responsive">
                <table class="table table-bordered table-striped"  id="mydata">
                    <thead>
                        <tr>
                            <th width='5%'>no</th>
                            <th width='15%'>nip</th>
                            <th width='20%'>nama</th>
                            <th width='10%'>Jenis Kelamin</th>
                            <th width='20%'>departemen</th>
                            <th>seksi</th>
                        </tr>
                    </thead>
                    <tbody>         
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    
    $(function () {
        console.log(new Date())
        //Date range picker
        $('#reservationdate').datetimepicker({
            format: 'MM/YYYY',
            minDate: new Date(), //set it here
        });
    })
    $('#view').hide();
    $("#dep").on("change", function() {
        // console.log(this.value)
        $('#view').hide();
        $.ajax({
            dataType: "json",
            type: 'POST', 
            url: '<?= base_url() ?>attshift/getSeksi',
            data: {
                dep: this.value        
            },
            success: function(data) {
                // console.log(data)
                var html =''
                html += '<option value="">-- PILIH SEKSI --</option>';
                for(var i=0; i<data.length; i++){
                    html += '<option value='+data[i].id_seksi+'>'+data[i].nama_seksi+'</option>'                   
                }
                $('#seksi').html(html)
            },
            error: function() {          
                error()
            }
        });
    });
    $("#seksi").on("change", function() {
        $('#view').hide();
    });
    $('#vw').click(function() {
        $('#view').toggle();
        // console.log($('#dep').val())
        table = $('#mydata').DataTable({
            "bDestroy": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?= base_url() ?>attshift/listPegawai",
                "type": "POST",
                "data" : {
                    "id_dep" : $('#dep').val(),
                    "id_seksi" : $('#seksi').val(),
                }
            },
        });
    });

    $(document).ready(function () { 
        $('#formGenerate').validate({
            rules: {
                    dep: {
                    required: true
                },
                    date: {
                    required: true,
                    minlength: 7
                },
                    opt1: {
                    required: true
                },
                    opt2: {
                    required: true
                },
                    opt3: {
                    required: true
                },
                    opt4: {
                    required: true
                }
            },
            messages: {
                    dep: {
                    required: "Pilih Departemen",
                },
                    date: {
                    required: "Pilih Bulan",
                    minlength: "Tidak sesuai"
                },
                    opt1: {
                    required: "Pilih Shift"
                },
                    opt2: {
                    required: "Pilih Shift"
                },
                    opt3: {
                    required: "Pilih Shift"
                },
                    opt4: {
                    required: "Pilih Shift"
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },

            submitHandler: function () {
                // console.log($('#date').val())
                $.LoadingOverlay("show");
                $.ajax({
                    dataType: "json",
                    type: 'POST', 
                    url: '<?= base_url() ?>attshift/generate',
                    data: {
                        dep: $('#dep').val(),
                        date: $('#date').val(),
                        opt1: $('#opt1').val(),
                        opt2: $('#opt2').val(),
                        opt3: $('#opt3').val(),
                        opt4: $('#opt4').val() ,
                        seksi: $('#seksi').val()        
                    },
                    success: function(response) {
                        if (response.result == 'Berhasil') {
                            successtr(response.message)
                        }else{
                            errortr(response.message)
                        }
                        $.LoadingOverlay("hide"); 
                    },
                    error: function() {          
                        error()
                    }
                });
            }
        });
    });
</script>