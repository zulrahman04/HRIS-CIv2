<?php
$base_url = base_url();
?>

<style type="text/css">
    .align-right{text-align:right;}
    .align-center{text-align:center;}
</style>

<div class="row">
	<div class="col-sm-12">
		<table id="typetable" class="table table-striped" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Kode</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
    	var kategori = "<?php Print($cat); ?>";

        table = $('#typetable').DataTable({
			"processing": true,
			"serverSide": true,
			"paging": true,
			"searching": true,
			"order": [],
			"ajax": {
				"url": "<?php echo base_url('index.php/item/ajax_listType')?>",
				"type": "POST",
				"data": {cat: kategori}
			}
        });

        new $.fn.dataTable.FixedHeader( table, {
			responsive: true,
			header: true,
			headerOffset: 0
		});

		$('#typetable tbody').on('click', 'tr', function () {
			var data = table.row( this ).data();
			var typecode = data[1];
			var typedesc = data[2];

			console.log(typecode);

			window.opener.document.getElementById('hidTypeCode').value = typecode;
			window.opener.document.getElementById('codedesc').value = typedesc;
			// window.opener.document.getElementById('uom').innerText = itmuom;
			// window.opener.document.getElementById('uomavail').innerText = itmuom;
			// window.opener.document.getElementById('qtyavail').value = availqty;
            //
			// window.opener.document.getElementById('hidItmCode').value = itmcode;

			window.close();
		} );
    });
</script>
