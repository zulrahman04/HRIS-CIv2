<?php
$base_url = base_url();
?>

<style type="text/css">
    .align-right{text-align:right;}
    .align-center{text-align:center;}
</style>

<div class="row">
	<div class="col-sm-12">
		<table id="itemtable" class="display compact" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Bin Code</th>
					<th>Bin Name</th>
                    <th>Qty</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 1; foreach ($bin as $key) {
					$qty = $this->db->select('qty')->from('item_detail')->where('bin_code', $key->bin_code)->where('item_code', $item)->get()->row('qty');
					?>
				<tr>
					<td><?= $no++ ?></td>
					<td><?= $key->bin_code ?></td>
					<td><?= $key->bin_name ?></td>
                    <td><?php if ($qty > 0) {
						echo $qty;
					}else{
						echo 0;
					}  ?></td>
				</tr>
				<?php
			} ?>

			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
		table = $('#itemtable').DataTable({
			"processing": true,
			"serverSide": false,
			"paging": true,
			"searching": true
		});
		$('#itemtable tbody').on('click', 'tr', function () {
			var data = table.row( this ).data();
			var bincode = data[1];
			var qty = data[3];
			// console.log(itmcode);

			window.opener.document.getElementById('bin').value = bincode;
			window.opener.document.getElementById('qty').value = qty;

			window.close();
		} );
    });
</script>
