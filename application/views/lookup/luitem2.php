<?php
$base_url = base_url();
?>

<style type="text/css">
    .align-right{text-align:right;}
    .align-center{text-align:center;}
</style>

<div class="row">
	<div class="col-sm-12">
		<table id="itemtable" class="table table-striped" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Item Code</th>
					<th>Description</th>
					<th>Category</th>
					<th>UoM</th>
					<th>Type</th>
					<th>Qty Avail</th>
                    <th>Qty Safety</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        table = $('#itemtable').DataTable({
			"processing": true,
			"serverSide": true,
			"paging": true,
			"searching": true,
			"order": [],
			"ajax": {
				"url": "<?php echo base_url('index.php/item/ajax_listItem2')?>",
				"type": "POST"
			},
			"columnDefs": [
				{
					"className": "align-right", "targets": [ 6, 7 ]
				},
                {
                    "targets": [ 3 ],
                    "visible": false
                }
			],
        });

        new $.fn.dataTable.FixedHeader( table, {
			responsive: true,
			header: true,
			headerOffset: 0
		});

		$('#itemtable tbody').on('click', 'tr', function () {
			var data = table.row( this ).data();
			var itmcode = data[1];
			var itmdesc = data[2];
			var itmuom  = data[4];
			var availqty  = data[6];
            var safetyqty = data[7];

			// console.log(itmcode);

			window.opener.document.getElementById('item_code').value = itmcode;
			window.opener.document.getElementById('description').value = itmdesc;
			window.opener.document.getElementById('uomsafety').innerText = itmuom;
			window.opener.document.getElementById('uomavail').innerText = itmuom;
            window.opener.document.getElementById('uomqty2pur').innerText = itmuom;
            window.opener.document.getElementById('uomqtypurch').innerText = itmuom;
			window.opener.document.getElementById('qtyavail').value = availqty;
            window.opener.document.getElementById('qtysafety').value = safetyqty;
            window.opener.document.getElementById('qty2pur').value = '0';

			window.opener.document.getElementById('hidItmCode').value = itmcode;

			window.close();
		} );
    });
</script>
