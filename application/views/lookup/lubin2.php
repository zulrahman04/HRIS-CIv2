<?php
$base_url = base_url();
?>

<style type="text/css">
    .align-right{text-align:right;}
    .align-center{text-align:center;}
</style>

<div class="row">
	<div class="col-sm-12">
		<table id="itemtable" class="display compact" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Bin Code</th>
                    <th>Bin Name</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1;
				foreach ($bin as $key) { 
					?>
				<tr>
					<td><?= $no++ ?></td>
					<td><?= $key->bin_code ?></td>
                    <td><?= $key->bin_name ?></td>
				</tr>
				<?php } ?>

			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
		table = $('#itemtable').DataTable({
			"processing": true,
			"serverSide": false,
			"paging": true,
			"searching": true
		});
		$('#itemtable tbody').on('click', 'tr', function () {
			var data = table.row( this ).data();
			var bincode = data[1];
			// console.log(itmcode);
        	var index = '<?= $index; ?>';
			index.split('_')[1]

			if (index.split('_')[0] == 'b2') {
				window.opener.document.getElementById('bin2_'+ index.split('_')[1]).value = bincode;	
				// console.log('bin2_'+ index.split('_')[1])		
			} else {
				window.opener.document.getElementById('bin_' + index.split('_')[1]).value = bincode;				
			}


			window.close();
		} );
    });
</script>
