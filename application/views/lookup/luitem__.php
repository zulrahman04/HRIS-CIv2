<?php
$base_url = base_url();
?>

<div class="row">
	<div class="col-sm-12">
		<table id="itemtable" class="table table-striped" width="100%">
			<thead>
				<tr>
					<th>Item Code</th>
					<th>Description</th>
					<th>Category</th>
					<th>UoM</th>
					<th>Type</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>

<script src="<?php echo base_url('assets/bower_components/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/bower_components/datatables-fixedheader314/js/dataTables.fixedHeader.js')?>"></script>
<script src="<?php echo base_url('assets/bower_components/datatables-fixedheader314/js/fixedHeader.bootstrap.min.js')?>"></script>

<script>
	$(document).ready(function() {
		var table = $('#itemtable').DataTable({
			"processing": true,
			"serverSide": true, 
			"paging": true,
			"searching": true,
			"order": [],
			"ajax": {
				"url": "<?php echo base_url('index.php/itmbrws/ajax_listItem')?>",
				"type": "POST"
			},
			"columnDefs": [{
				"targets": [ -1 ], 
				"orderable": false, 
			},
			],
		});
		new $.fn.dataTable.FixedHeader( table, {
			responsive: true,
			header: true
			// headerOffset: 20
		});

		$('#itemtable tbody').on('click', 'tr', function () {
			var data = table.row( this ).data();
			var itmcode = data[0];
			var itmdesc = data[1];
			var itmuom  = data[3];

			window.opener.document.getElementById('item_code').value = itmcode;
			window.opener.document.getElementById('description').value = itmdesc;
			window.opener.document.getElementById('uom').value = itmuom;

			// window.opener.document.getElementById('hidItmCode').value = itmcode;

			window.close();
			
			// getItem2(itmcode, itmdesc, itmuom);
		} );
		
		// function getItem2(itmcode, itmdesc, itmuom) {
		// 	window.opener.document.getElementById('item_code').value = itmcode;
		// 	window.opener.document.getElementById('description').value = itmdesc;
		// 	window.opener.document.getElementById('uom').value = itmuom;

		// 	window.opener.document.getElementById('hidItmCode').value = itmcode;

		// 	window.close();
		// }

	});

	
</script>
