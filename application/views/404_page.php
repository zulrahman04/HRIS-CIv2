
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>HRIS-V2</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
    
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/fontawesome-free/css/all.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="<?= base_url('assets') ?>/dist/css/adminlte.min.css">
    </head>


    <body>  
        <div class="wrapper">
            <div>
                <!-- Content Header (Page header) -->
                <section class="content-header">
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="error-page">
                        <h2 class="headline text-warning"> 404</h2>

                        <div class="error-content">
                            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>

                            <p>
                                We could not find the page you were looking for.
                                Meanwhile, you may <a href="<?= base_url() ?>">return to dashboard</a>
                            </p>
                        </div>
                        <!-- /.error-content -->
                    </div>
                <!-- /.error-page -->
                </section>
                <!-- /.content -->
            </div>
        </div>
        <!-- jQuery -->
        <script src="<?= base_url('assets') ?>/plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<?= base_url('assets') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url('assets') ?>/dist/js/adminlte.min.js"></script>
    </body>
</html>