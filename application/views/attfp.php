

    <div class="card">
      <div class="card-header">
        <h3 class="card-title"><?= $pagetitle ?></h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped"  id="mydata">
            <thead>
              <tr>
                <th>nip</th>
                <th>nama</th>
                <th>time</th>
                <th>type</th>
                <th>Jenis Kelamin</th>
              </tr>
            </thead>
            <tbody id="show_data">
              <?php foreach ($data as $key) { ?>           
              <tr>
                <td><?= $key->ssn ?></td>
                <td><?= $key->name ?></td>
                <td><?= $key->CHECKTIME ?></td>
                <td><?= $key->CHECKTYPE ?></td>
                <td><?= $key->gender ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">
        <button class="btn btn-sm btn-info float-left" onclick="importabsensi()">Import Absensi</button>
        <a href="<?= base_url('/attendance')?>" class="btn btn-sm btn-secondary float-right">View All Absensi</a>
      </div>
      <!-- /.card-footer -->
    </div>
  <!-- /.card --> 
<!-- /.content -->

<script>
  $(function () {
    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });
  })
  $("#mydata").DataTable({
    "responsive": true,
    "autoWidth": false,
  });

  function importabsensi(){
    $.LoadingOverlay("show");
    $.ajax({
      dataType: "json",
			type: 'POST', 
			url: '<?= base_url() ?>attfp/import',
			success: function(response) { 
        if (response.result == 'Berhasil') {                 
          successtr(response.message)
        }else{
          errortr(response.message)
        }
        $.LoadingOverlay("hide");
			},
      error: function() {        
        error()
      }
		});
  }

</script>