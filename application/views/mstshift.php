
    <div class="card">
      <div class="card-header">
        <button class="btn btn-primary" onclick="formAdd()"><i class="nav-icon fa fa-fw fa-plus"></i> Tambah</button>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped"  id="mydata">
            <thead>
              <tr>
                <th width='5px'>No</th>
                <th>Kode Shift</th>
                <th>IN</th>
                <th>OUT</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <?php $no = 1;
                    foreach ($shift as $key) {?>
                    <tr>
                      <td><?= $no++?></td>
                      <td><?= $key->shift?></td>
                      <td><?= $key->in?></td>
                      <td><?= $key->out?></td>
                      <td>
                        <button class="btn btn-info btn-sm" onclick="formEditShift(<?= $key->id ?>)"><i class="fa fa-fw fa-edit"></i></button>
                        <button class="btn btn-danger btn-sm" onclick="deleteShift(<?= $key->id ?>)"><i class="fa fa-fw fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.card-body -->
      <!-- /.card-footer -->
    </div>
  <!-- /.card --> 
<!-- /.content -->

<div class="modal fade" id="modal-add">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Form Tambah Shift</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="form-horizontal" id="frmNewShift">
          <div class="modal-body">
            <div class="form-body">
                <div class="form-group">
                    <label for="code">Code
                        <span class="text-danger"> * </span>
                    </label>
                    <input type="text" id="code" name="code" onkeyup="Capital()" class="form-control"/>
                  </select>
                </div>
                <div class="form-group">
                    <label for="in">IN
                        <span class="text-danger"> * </span>
                    </label>
                    <input type="time" id="in" name="in" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="out">OUT
                        <span class="text-danger"> * </span>
                    </label>
                    <input type="time" id="out" name="out" class="form-control"/>
                </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button class="btn btn-primary" onclick="saveAddChild()">Save changes</button>
          </div>
        </form>
    </div>
      <!-- /.modal-content -->
  </div>
</div>

<div class="modal fade" id="modal-edit">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit Shift</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="form-horizontal" id="frmEditShift">
          <div class="modal-body">
            <div class="form-body">
              <div class="form-group">
                  <label for="code2">Code
                      <span class="text-danger"> * </span>
                  </label>
                  <input type="hidden" id="id" name="id" class="form-control"/>
                  <input type="text" id="code2" name="code2" onkeyup="Capital()" class="form-control"/>
              </div>
              <div class="form-group">
                  <label for="in2">IN
                      <span class="text-danger"> * </span>
                  </label>
                  <input type="time" id="in2" name="in2" class="form-control"  />
              </div>
              <div class="form-group">
                  <label for="out2">OUT
                      <span class="text-danger"> * </span>
                  </label>
                  <input type="time" id="out2" name="out2" class="form-control"/>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" >Save changes</button>
          </div>
        </form>
    </div>
      <!-- /.modal-content -->
  </div>
</div>

<script>
  $(document).ready(function () { 
    $('#frmNewShift').validate({
      rules: {
            code: {
              required: true
          },
            in: {
              required: true
          },
            out: {
              required: true
          }
      },
      messages: {
            code: {
              required: "Masukan Code",
          },
            in: {
              required: "Masukan Jam Masuk"
          },
            out: {
              required: "Masukan Jam Keluar"
          }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      },

      submitHandler: function () {
        $.ajax({
          dataType: "json",
          type: 'POST', 
          url: '<?= base_url() ?>mstshift/addShift',
          data: {
            code: $('#code').val(),
            in: $('#in').val(),
            out: $('#out').val()       
          },
          success: function(response) {
            if (response.result == 'Berhasil') {
              successtr(response.message)
            }else{
              errortr(response.message)
            } 
          },
          error: function() {          
            error()
          }
        });
      }
    });
    $('#frmEditShift').validate({
      rules: {
            code2: {
              required: true
          },
            in2: {
              required: true
          },
            out2: {
              required: true
          }
      },
      messages: {
            code2: {
              required: "Masukan Nama Code",
          },
            in2: {
              required: "Masukan Jam Masuk"
          },
            out2: {
              required: "Masukan Jam Keluar"
          }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      },
      
      submitHandler: function () {
        $.ajax({
          dataType: "json",
          type: 'POST', 
          url: '<?= base_url() ?>mstshift/saveEditShift',
          data: {
            id: $('#id').val(),
            code: $('#code2').val(),
            in: $('#in2').val(),
            out: $('#out2').val()
          },
          success: function(response) { 
            if (response.result == 'Berhasil') {                 
              successtr(response.message)
            }else{
              errortr(response.message)
            } 
          },
          error: function() {          
            error()
          }
        });
      }
    });
  });



    function formAdd(){
        $('#code').val('')
        $('#in').val('')
        $('#out').val('')
        $('#modal-add').modal()
    }

    function formEditShift(id){
        $('#code2').val('')
        $('#in2').val('')
        $('#out2').val('')
        $('#modal-edit').modal()
        $.ajax({
            dataType: "json",
            type: 'POST', 
            url: '<?= base_url() ?>mstshift/getShiftEdit',
            data: {
                id: id
            },
            success: function(response) { 
                $('#id').val(response.id)
                $('#code2').val(response.shift)
                $('#in2').val(response.in)
                $('#out2').val(response.out)
            },
            error: function() {
                error()
            }
        });
    }

    function Capital() {
        var x = document.getElementById("code");
        x.value = x.value.toUpperCase();

        var y = document.getElementById("code2");
        y.value = y.value.toUpperCase();        
    }
    function deleteShift(id){
        Swal.fire({
        title: 'Anda yakin?',
        text: "Anda tidak bisa mengembalikannya!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) { 
            $.ajax({
            dataType: "json",
            type: 'POST', 
            url: '<?= base_url() ?>mstshift/deleteShift',
            data: {
                id: id
            },
            success: function(response) { 
                if (response.result == 'Berhasil') { 
                successtr(response.message)
                }else{
                errortr(response.message)
                }
            },
            error: function() {  
                error()
            }
            });
        }
        })
    }

    $("#mydata").DataTable({
        "responsive": true,
        "autoWidth": false,
    });
</script>