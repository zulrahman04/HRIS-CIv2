
    <div class="card">
      <div class="card-header">
        <form action="<?= base_url('attendance')?>" method="post">
          <div class="col-12 row">
            <div class="col-2">
              <label for="dep">Departemen</label>
                <select class="form-control select2" id="dep" name="dep">
                  <option value="">-- Semua Departemen --</option>
                  <?php foreach ($depall as $depa) {
                    if ($depa->id_departemen == $depart) {?>
                      <option value="<?= $depa->id_departemen?>" selected><?= $depa->nama_departemen?></option>
                    <?php }else{?>
                      <option value="<?= $depa->id_departemen?>"><?= $depa->nama_departemen?></option>
                  <?php } }?>
                </select>
            </div>
            <div class="col-2">
              <label for="seksi">Seksi</label>
                <select class="form-control select2" id="seksi" name="seksi">
                  <option value="">-- Semua Seksi --</option>
                  <?php foreach ($seksiall as $se) {
                    if ($se->id_seksi == $seksi) {?>
                      <option value="<?= $se->id_seksi?>" selected><?= $se->nama_seksi?></option>
                    <?php }else{?>
                      <option value="<?= $se->id_seksi?>"><?= $se->nama_seksi?></option>
                  <?php } }?>
                </select>
            </div>
            <div class="col-2">
              <label for="date">Tanggal</label>
              <div class="input-group mb-3" id="reservationdate" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" id="date" name="date" value="<?= $date ?>"/>
                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>  
              </div>
            </div>
            <div class="col-2">
              <label for="" style="color:#FFFFFF ;">SSM</label>
              <div>
                <button class="btn btn-primary" type="submit">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped "  id="mydata">
            <thead>
              <tr>
                <th>no</th>
                <th>nip</th>
                <th>nama</th>
                <th>Gender</th>
                <th>departemen</th>
                <th>seksi</th>
                <th>shift</th>
                <th>Sch in</th>
                <th>Sch out</th>
                <th>IN</th>
                <th>OUT</th>
                <th>code</th>
                <!-- <th>action</th> -->
              </tr>
            </thead>
            <tbody>
              <?php $no = 1;
              foreach ($data as $key) { ?>           
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $key->nip ?></td>
                <td><?= $key->nama ?></td>
                <td><?= $key->jk ?></td>
                <td><?= $key->nama_departemen ?></td>
                <td><?= $key->nama_seksi ?></td>
                <td width="9%"><select class="form-control select2" id="shift_<?=$key->id ?>" name="shift_<?=$key->id ?>" onchange="shift('shift_<?=$key->id ?>_'+this.value)">
                    <?php foreach ($shift as $sh) {
                      if ($key->shift == $sh->shift) { ?>
                        <option value='<?= $sh->shift ?>' selected><?= $sh->shift ?></option>                                        
                      <?php }else{?>
                        <option value='<?= $sh->shift ?>' ><?= $sh->shift ?></option>
                      <?php } 
                    } ?>
                  </select>                  
                </td>
                <td><?= $key->schedulein ?></td>
                <td><?= $key->scheduleout ?></td>         
                <td width="6%">
                  <input type="time" id="in_<?=$key->id ?>" name="in_<?=$key->id ?>" value="<?= $key->masuk?>" class="form-control" onchange="masuk(event)"/>           
                </td>                
                <td width="6%">
                  <input type="time" id="out_<?=$key->id ?>" name="out_<?=$key->id ?>" value="<?= $key->pulang?>" class="form-control" onchange="pulang(event)"/> 
                </td>  
                <td width="8%">
                  <select class="form-control select2" id="code_<?=$key->id ?>" name="code_<?=$key->id ?>" onchange="code('code_<?=$key->id ?>_'+this.value)">
                      <option value=''>Pilih</option>
                      <?php foreach ($code as $cd) {
                      if ($key->att_code == $cd->code) { ?>
                        <option value='<?= $cd->code ?>' selected><?= $cd->code ?></option>                                        
                      <?php }else{?>
                        <option value='<?= $cd->code ?>' ><?= $cd->code ?></option>
                      <?php } 
                    } ?>
                  </select>  </td>
                <!-- <td>
                    <button class="btn btn-info btn-sm" onclick="formEdit(<?= $key->id?>)" ><i class="fa fa-fw fa-edit"></i></button>
                </td>   -->
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.card-body -->
    </div>
  <!-- /.card --> 
<!-- /.content -->

<div class="modal fade" id="modal-editAtt">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Form Edit Attendance</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <form class="form-horizontal" id="frmEditAtt">
            <div class="modal-body">
              <div class="form-body">
                  <div class="form-group">
                      <label for="nip">NIP
                          <span class="text-danger"> * </span>
                      </label>
                      <input type="hidden" id="id" name="id" class="form-control"/>
                      <input type="text" id="nip" name="nip" class="form-control" readonly/>
                  </div>
                  <div class="form-group">
                      <label for="nama">nama
                          <span class="text-danger"> * </span>
                      </label>
                      <input type="text" id="nama" name="nama" class="form-control"  readonly/>
                  </div>
                  <div class="form-group">
                      <label for="dep">Departemen
                          <span class="text-danger"> * </span>
                      </label>
                      <input type="text" id="dep" name="dep" class="form-control" readonly/>
                    </div>
                    <div class="form-group">
                      <label>Shift</label>
                      <select class="form-control select2" id="shift" name="shift">
                      </select>
                    </div>
                    <div class="form-group">
                        <label for="n=in">IN</label>
                        <input type="time" id="in" name="in" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="out">OUT</label>
                        <input type="time" id="out" name="out" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label>Code Attendance
                            <span class="text-danger"> * </span>
                      </label>
                      <select class="form-control select2" id="code" name="code">
                      </select>
                    </div>
              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </form>
      <!-- /.modal-content -->
  </div>
</div>

<script>
  $(function () {
    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    })
    $("#mydata").DataTable({
      "responsive": true,
      "autoWidth": false,
    });

    function formEdit(id){
      $('#id').val('')
      $('#nip').val('')
      $('#nama').val('')
      $('#dep').val('')
      $('#shift').val('')
      $('#code').val('')
      $('#in').val('')
      $('#out').val('')
      $('#modal-editAtt').modal()
      $.ajax({
        dataType: "json",
        type: 'POST', 
        url: '<?= base_url() ?>attendance/getAtt',
        data: {
          id: id
        },
        success: function(data) { 
          console.log(data)
          $('#id').val(data.id)
          $('#nip').val(data.nip)
          $('#nama').val(data.nama)
          $('#dep').val(data.nama_departemen)
          $('#in').val(data.masuk)
          $('#out').val(data.pulang)
          shift(data.shift)
          codeatt(data.att_code)
        },
        error: function() {          
          Swal.fire({
              icon: 'error',
              title: 'Terjadi kesalahan.',
              showConfirmButton: false,
              timer: 1500
          })
          reload()
        }
      });  
    }

    function shift(code){
      $.ajax({
        dataType: "json",
        type: 'POST', 
        url: '<?= base_url() ?>attendance/getShift',
        success: function(data) {
          // console.log(data) 
          var html =''
          html += '<option value="">-- Pilih Shift --</option>';
          for(var i=0; i<data.length; i++){
            if (code == data[i].shift) {
              html += '<option value='+data[i].shift+' selected>'+data[i].shift+'  '+data[i].in+' - '+data[i].out+'</option>';            
            }else{
              html += '<option value='+data[i].shift+'>'+data[i].shift+'  '+data[i].in+' - '+data[i].out+'</option>';    
            }
          }
          $('#shift').html(html)
        },
        error: function() {          
          Swal.fire({
              icon: 'error',
              title: 'Terjadi kesalahan.',
              showConfirmButton: false,
              timer: 1500
          })
          reload()
        }
      });  
    }

    function codeatt(code){
      $.ajax({
        dataType: "json",
        type: 'POST', 
        url: '<?= base_url() ?>attendance/getCodeAtt',
        success: function(data) {
          // console.log(data) 
          var html =''
          html += '<option value="">-- Pilih Code --</option>';
          for(var i=0; i<data.length; i++){
            if (code == data[i].code) {
              html += '<option value='+data[i].code+' selected>'+data[i].code+' - '+data[i].description+'</option>';            
            }else{
              html += '<option value='+data[i].code+'>'+data[i].code+' - '+data[i].description+'</option>';    
            }
          }
          $('#code').html(html)
        },
        error: function() {          
          Swal.fire({
              icon: 'error',
              title: 'Terjadi kesalahan.',
              showConfirmButton: false,
              timer: 1500
          })
          reload()
        }
      });  
    }

    $(document).ready(function () { 
      $('#frmEditAtt').validate({
      rules: {
            code: {
              required: true
          }
      },
      messages: {
              code: {
              required: "Pilih Code"
          }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
      },

      submitHandler: function () {
        $.ajax({
          dataType: "json",
          type: 'POST', 
          url: '<?= base_url() ?>attendance/updAtt',
          data: {
            id : $('#id').val(),
            shift : $('#shift').val(),
            code : $('#code').val(),
            in : $('#in').val(),
            out : $('#out').val()     ,  
          },
          success: function(response) {
            if (response.result == 'Berhasil') {
              successtr(response.message)
            }else{
              errortr(response.message)
            } 
          },
          error: function() {          
            // error()
          }
        });
      }
    });
  });
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

  $("#dep").on("change", function() {
        // console.log(this.value)
        $('#view').hide();
        $.ajax({
            dataType: "json",
            type: 'POST', 
            url: '<?= base_url() ?>attshift/getSeksi',
            data: {
                dep: this.value        
            },
            success: function(data) {
                // console.log(data)
                var html =''
                html += '<option value="">-- Semua Seksi --</option>';
                for(var i=0; i<data.length; i++){
                  html += '<option value='+data[i].id_seksi+'>'+data[i].nama_seksi+'</option>'
                }
                $('#seksi').html(html)
            },
            error: function() {          
                error()
            }
        });
    });

  function shift(sh){
    var shi = sh.split("_")
    var id = shi[1]
    var shift = shi[2]
    console.log(id)

    $.ajax({
      dataType: "json",
      type: "POST",
      url: "<?= base_url()?>attendance/chgshift",
      data: {
          id: id,
          shift:shift
      },
      success: function(response) {
        if (response.result == 'Berhasil') {
          Toast.fire({
              icon: 'success',
              title: response.message
          })          
        }else{
          Toast.fire({
              icon: 'error',
              title: response.message
          }) 
        }
      },
      error: function() {
          error()
      }
    });
  }

  function code(cd){
    var cod = cd.split("_")
    var id = cod[1]
    var code = cod[2]

    $.ajax({
      dataType: "json",
      type: "POST",
      url: "<?= base_url()?>attendance/chgcode",
      data: {
          id: id,
          code:code
      },
      success: function(response) {
        if (response.result == 'Berhasil') {
          Toast.fire({
              icon: 'success',
              title: response.message
          })        
        }else{
          Toast.fire({
              icon: 'error',
              title: response.message
          }) 
        }
      },
      error: function() {
        error()
      }
    });
  }

  function masuk(e){
    var msk = e.target.id.split("_")
    var id = msk[1]

    $.ajax({
      dataType: "json",
      type: "POST",
      url: "<?= base_url()?>attendance/chgmsk",
      data: {
          id: id,
          masuk:e.target.value
      },
      success: function(response) {
        if (response.result == 'Berhasil') {
          Toast.fire({
              icon: 'success',
              title: response.message
          })        
        }else{
          Toast.fire({
              icon: 'error',
              title: response.message
          }) 
        }
      },
      error: function() {
        error()
      }
    });
  }

  function pulang(e){
    var plg = e.target.id.split("_")
    var id = plg[1]

    $.ajax({
      dataType: "json",
      type: "POST",
      url: "<?= base_url()?>attendance/chgplg",
      data: {
          id: id,
          plg:e.target.value
      },
      success: function(response) {
        if (response.result == 'Berhasil') {
          Toast.fire({
              icon: 'success',
              title: response.message
          })        
        }else{
          Toast.fire({
              icon: 'error',
              title: response.message
          }) 
        }
      },
      error: function() {
        error()
      }
    });
  }
</script>