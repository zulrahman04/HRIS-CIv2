<?php
class Attshift_model extends MY_Model {


    public function getDept(){
        return $this->db->select('*')->from('departemen')->get()->result();
    }

    public function getShift(){
        return $this->db->select('*')->from('mst_shift')->get()->result();
    }

    public function getShiftSelect($id){
        return $this->db->select('*')->from('seksi')->where('id_departemen', $id)->get()->result();
    }

    public function cekGenerate($dep,$bln,$thn){
        return $this->db->query("SELECT nip, nama, id_departemen, id_seksi, jk
                                FROM pegawai
                                WHERE flag = 'A' 
                                AND id_departemen = '$dep' 
                                AND nip NOT IN (SELECT nip FROM attendance 
                                WHERE MONTH(DATE) = '$bln' AND YEAR(DATE)= '$thn')")->result();
    }

    public function generateAttShift($dep,$fdate,$ldate,$opt1,$opt2,$opt3,$opt4,$seksi){
        
        $begin = new DateTime($fdate);
        $end = new DateTime($ldate);
        
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        
        $shift = '';
        $this->db->trans_begin();
        foreach ($period as $dt) {
            $date = $dt->format("Y-m-d");
            
            $cekoff = $this->db->select('tanggal')->from('mst_dayoff')->where('tanggal', $date)->get()->row_array();
            if ($cekoff) {
                $shift = 'DAYOFF';
            }else{
                if ($dt->format("l") == 'Monday' || $dt->format("l") == 'Tuesday' ||$dt->format("l") == 'Wednesday' ||$dt->format("l") == 'Thursday') {
                    $shift = $opt1;
                }elseif ($dt->format("l") == 'Friday') {
                    $shift = $opt2;
                }elseif ($dt->format("l") == 'Saturday') {
                    $shift = $opt3;
                }elseif ($dt->format("l") == 'Sunday') {
                    $shift = $opt4;
                }
            }
            
            $shiftinout = $this->db->select('in,out')->from('mst_shift')->where('shift', $shift)->get()->row_array();
            $in = $shiftinout['in'];
            $out = $shiftinout['out'];
            $user = $this->session->userdata('name');

            if ($seksi) {
                $this->db->query("INSERT INTO attendance (nip, nama, id_departemen, id_seksi,jk,shift,date,schedulein,scheduleout,created_by)
                            SELECT nip, nama, id_departemen, id_seksi, jk, '$shift', '$date', '$in', '$out', '$user'
                            FROM pegawai
                            WHERE flag = 'A' and id_departemen = '$dep' and id_seksi = '$seksi'
                            and nip not in (SELECT nip FROM attendance WHERE DATE = '$date')");
            }else{
                $this->db->query("INSERT INTO attendance (nip, nama, id_departemen, id_seksi,jk,shift,date,schedulein,scheduleout,created_by)
                            SELECT nip, nama, id_departemen, id_seksi, jk, '$shift', '$date', '$in', '$out', '$user'
                            FROM pegawai
                            WHERE flag = 'A' and id_departemen = '$dep'
                            and nip not in (SELECT nip FROM attendance WHERE DATE = '$date')");
            }
        }

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

    var $order = array('id_pegawai' => 'asc', 'nip' => 'asc', 'nama' => 'asc', 'nama_seksi' => 'asc', 'nama_departemen' => 'asc'); 
    var $column_order = array('id_pegawai','nip', 'nama', 'jk', 'nama_departemen', 'nama_seksi'); 
    var $column_search = array('nip', 'nama', 'jk', 'nama_seksi', 'nama_departemen');

    function _get_datatables_query($id_dep, $id_seksi){
        $this->db->select('p.id_jabatan, p.nip, p.nama, p.tgl_lahir, p.tempat_lahir, p.jk, p.agama, p.status_kerja, p.status_kawin, p.no_telp, j.nama_jabatan, d.nama_departemen,s.nama_seksi');
        $this->db->from('pegawai p');
        $this->db->join('jabatan j', 'p.id_jabatan = j.id_jabatan', 'left');
        $this->db->join('departemen d', 'p.id_departemen = d.id_departemen', 'left');
        $this->db->join('seksi s', 'p.id_seksi = s.id_seksi', 'left');
        $this->db->where('p.id_departemen', $id_dep);
        // $this->db->where('tgl_akhir_kerja = "0000-00-00"');
        $this->db->where('flag = "A"');
        if ($id_seksi) {
            $this->db->where('p.id_seksi', $id_seksi);
        }
        $i = 0;
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0) {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($id_dep, $id_seksi){
        $this->_get_datatables_query($id_dep, $id_seksi);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        
        return $query->result();
    }

    function count_filtered($id_dep, $id_seksi){
        $this->_get_datatables_query($id_dep, $id_seksi);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all($id_dep, $id_seksi){
        $this->db->select('p.id_jabatan, p.nip, p.nama, p.tgl_lahir, p.tempat_lahir, p.jk, p.agama, p.status_kerja, p.status_kawin, p.no_telp, j.nama_jabatan, d.nama_departemen,s.nama_seksi');
        $this->db->from('pegawai p');
        $this->db->join('jabatan j', 'p.id_jabatan = j.id_jabatan', 'left');
        $this->db->join('departemen d', 'p.id_departemen = d.id_departemen', 'left');
        $this->db->join('seksi s', 'p.id_seksi = s.id_seksi', 'left');
        $this->db->where('p.id_departemen', $id_dep);
        // $this->db->where('tgl_akhir_kerja = "0000-00-00"');
        $this->db->where('flag = "A"');
        if ($id_seksi) {
            $this->db->where('p.id_seksi', $id_seksi);
        }
        return $this->db->count_all_results();
    }

}