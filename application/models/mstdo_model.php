<?php
class Mstdo_model extends MY_Model {
    public function getDo(){
        return $this->db->select('*')->from('mst_dayoff')->get()->result();
    }

    public function saveAddDo($tgl,$ket){

        $this->db->trans_begin();
        $data = array(  'tanggal' => $tgl,
                        'keterangan' => $ket
                    );
        $this->db->insert('mst_dayoff', $data);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

    public function cekDo($tgl){
        return $this->db->select('*')->from('mst_dayoff')->where('tanggal', $tgl)->get()->row();
    }

    public function saveEditDo($id,$tgl,$ket){

        $this->db->trans_begin();
        $data = array(  'tanggal' => $tgl,
                        'keterangan' => $ket
                    );
        $this->db->where('id', $id);
        $this->db->update('mst_dayoff', $data);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

    public function deleteDo($id){

        $this->db->trans_begin();

        $this->db->where('id', $id);
        $this->db->delete('mst_dayoff');

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }
}