<?php
class Overtime_model extends MY_Model
{

    var $order = array('no_spl' => 'asc'); 
    var $column_order = array('no_spl', 'tgl_pengajuan', 'nama'); 
    var $column_search = array('no_spl', 'tgl_pengajuan', 'nama');
    function _get_datatables_query(){
        $this->db->select('a.no_spl, a.tgl_pengajuan,
                           a.nip, a.nama,
                           b.nama_departemen, c.nama_seksi, a.mengetahui_hrd, status_approval_form')
                 ->from('spl_header a')
                 ->join('departemen b', 'a.id_departemen = b.id_departemen', 'left')
                 ->join('seksi c', 'c.id_seksi = a.id_seksi and c.id_departemen = b.id_departemen', 'left')
                 ->order_by('a.no_spl', 'desc');
        $i = 0;
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0) {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
		
        return $query->result();
    }

    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all(){
        $this->db->select('a.no_spl, a.tgl_pengajuan,
        a.nip, a.nama,
        b.nama_departemen, c.nama_seksi, a.mengetahui_hrd')
        ->from('spl_header a')
        ->join('departemen b', 'a.id_departemen = b.id_departemen', 'left')
        ->join('seksi c', 'c.id_seksi = a.id_seksi and c.id_departemen = b.id_departemen', 'left')
        ->order_by('a.no_spl', 'desc');
        return $this->db->count_all_results();
    }
}