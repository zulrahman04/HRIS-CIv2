<?php
class Attfp_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $CI = &get_instance();
        $this->db_FP = $CI->load->database('db2', TRUE);
    }

    public function getFingerPrint(){
		$tgl = date('Y-m-d', strtotime('-7 days', strtotime( date('Y-m-d'))));
        $sql = "SELECT name, gender, ssn, user.`USERID`, CHECKTIME, CHECKTYPE
                FROM USERINFO user
                INNER JOIN CHECKINOUT ON user.USERID = CHECKINOUT.USERID 
                WHERE FORMAT(CHECKTIME, 'yyyy-mm-dd') >= '$tgl'
                AND WorkCode = '0'
                order by CHECKTIME asc";
        $query = $this->db_FP->query($sql);
        return $query->result();
    }

    public function import(){
        
        $this->db->trans_begin();
        $tglnow = date('Y-m-d');
		$tgl = date('Y-m-d', strtotime('-7 days', strtotime( date('Y-m-d'))));
        $sql = "SELECT name, ssn, CHECKTIME, CHECKTYPE
                FROM USERINFO user
                INNER JOIN CHECKINOUT ON user.USERID = CHECKINOUT.USERID 
                WHERE FORMAT(CHECKTIME, 'yyyy-mm-dd') >= '$tgl'
                AND WorkCode = '0'
                ";
        $query = $this->db_FP->query($sql)->result();  
        foreach ($query as $key) {
            $nama = str_replace("'","",$key->name);
            $data = array(  
                            'nama' => $nama,
                            'nip' => $key->ssn,
                            'time' => $key->CHECKTIME,
                            'type' => $key->CHECKTYPE);
            $this->db->insert('checkinout', $data);

        }

        $pgw = $this->db->query("SELECT id,date FROM attendance WHERE DATE BETWEEN '$tgl' AND '$tglnow'")->result();

        foreach ($pgw as $key) {
            $att = $this->db->query("SELECT 
                            a.id,
                            a.nama, 
                            c.nip, 
                            (SELECT MAX(TIME) FROM checkinout WHERE DATE(TIME) = '$key->date' 
                                AND TYPE = 'I' 
                                AND SUBSTRING(nip, 1, 10) = SUBSTRING(c.nip, 1, 10)
                            ) AS masuk, 
                            IF(
                                d.nama_departemen = 'PRODUCTION', 
                                (SELECT MIN(TIME) FROM checkinout WHERE TIME BETWEEN (
                                        SELECT MIN(TIME) FROM checkinout WHERE 
                                        DATE(TIME) = '$key->date' AND TYPE = 'I' 
                                        AND SUBSTRING(nip, 1, 10) = SUBSTRING(c.nip, 1, 10)
                                    ) 
                                    AND (SELECT MAX(TIME) FROM checkinout WHERE DATE(TIME) = '$key->date' 
                                        AND TYPE = 'I' AND SUBSTRING(nip, 1, 10) = SUBSTRING(c.nip, 1, 10)
                                    ) + INTERVAL '14' HOUR 
                                    AND TYPE = 'O' AND SUBSTRING(nip, 1, 10) = SUBSTRING(c.nip, 1, 10)
                                ), 
                                (SELECT MAX(TIME) FROM checkinout WHERE 
                                    DATE(TIME) = '$key->date' AND TYPE = 'O' 
                                    AND SUBSTRING(nip, 1, 10) = SUBSTRING(c.nip, 1, 10)
                                )
                            ) AS pulang FROM checkinout c 
                            INNER JOIN attendance a ON SUBSTRING(c.nip, 1, 10) = SUBSTRING(a.nip, 1, 10) 
                            LEFT JOIN departemen d ON (a.id_departemen = d.id_departemen) 
                            WHERE DATE(TIME) = '$key->date' and a.id = '$key->id' GROUP BY c.nip, a.nama, masuk, a.id, pulang")->row_array();
            // echo $att['nip'].' - '. $att['nama'].' - '. $att['masuk'].' - '. $att['pulang'].'<br>';

            if ($att['masuk']) {
                $masuk =  date('H:i:s', strtotime($att['masuk']));
                $data = array(  
                            'checkin' => $masuk,
                            'att_code' => 'H',
                            'update_by' => $this->session->userdata('name'));
                $this->db->where("checkin is null");  
                $this->db->where("id", $att['id']);
                $this->db->update('attendance', $data);
            }
            if ($att['pulang']) {
                $pulang =  date('H:i:s', strtotime($att['pulang']));
                $data2 = array(  
                            'checkout' => $pulang,
                            'att_code' => 'H',
                            'update_by' => $this->session->userdata('name'));
                $this->db->where("checkout is null");  
                $this->db->where("id", $att['id']);
                $this->db->update('attendance', $data2);
            }
        }
        // $sql = "UPDATE CHECKINOUT 
   		// 		SET WorkCode = '1'
   		// 		WHERE FORMAT(CHECKTIME, 'yyyy-mm-dd') >= '$tgl'
   		// 		AND WorkCode = '0'";
        // $this->db_FP->query($sql);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
	}
}
