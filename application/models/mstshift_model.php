<?php
class Mstshift_model extends MY_Model {
    public function getShift(){
        return $this->db->select('*')->from('mst_shift')->get()->result();
    }

    public function saveAddShift($code,$in,$out){

        $this->db->trans_begin();
        $data = array(  'shift' => $code,
                        'in' => $in,
                        'out' => $out
                    );
        $this->db->insert('mst_shift', $data);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

    public function getShiftEdit($id){
        return $this->db->select('*')->from('mst_shift')->where('id', $id)->get()->row();
    }

    public function saveEditShift($id,$code,$in,$out){

        $this->db->trans_begin();

        $data = array(  'shift' => $code,
                        'in' => $in,
                        'out' => $out
                    );
        $this->db->where('id', $id);
        $this->db->update('mst_shift', $data);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

    public function deleteShift($id){

        $this->db->trans_begin();

        $this->db->where('id', $id);
        $this->db->delete('mst_shift');

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }
}