<?php
class Pegawai_model extends MY_Model 
{
    var $order = array('id_pegawai' => 'asc'); 
    var $column_order = array('nip', 'nama', 'tgl_lahir', 'tempat_lahir', 'jk','agama','status_kerja', 'status_kawin', 'no_telp', 'nama_jabatan', 'nama_departemen'); 
    var $column_search = array('nip', 'nama', 'tgl_lahir', 'tempat_lahir', 'jk','agama','status_kerja', 'status_kawin', 'no_telp', 'nama_jabatan', 'nama_departemen');

    function _get_datatables_query(){
        $this->db->select('p.id_jabatan, p.nip, p.nama, p.tgl_lahir, p.tempat_lahir, p.jk, p.agama, p.status_kerja, p.status_kawin, p.no_telp, j.nama_jabatan, d.nama_departemen,s.nama_seksi')
                    ->from('pegawai p')
                    ->join('jabatan j', 'p.id_jabatan = j.id_jabatan')
                    ->join('departemen d', 'p.id_departemen = d.id_departemen')
                    ->join('seksi s', 'p.id_seksi = s.id_seksi', 'left')
                    ->where('tgl_akhir_kerja = "0000-00-00"');
        $i = 0;
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0) {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        
        return $query->result();
    }

    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all(){
        $this->db->select('p.id_jabatan, p.nip, p.nama, p.tgl_lahir, p.tempat_lahir, p.jk, p.agama, p.status_kerja, p.status_kawin, p.no_telp, j.nama_jabatan, d.nama_departemen,s.nama_seksi')
                    ->from('pegawai p')
                    ->join('jabatan j', 'p.id_jabatan = j.id_jabatan')
                    ->join('departemen d', 'p.id_departemen = d.id_departemen')
                    ->join('seksi s', 'p.id_seksi = s.id_seksi')
                    ->where('tgl_akhir_kerja = "0000-00-00"');
        return $this->db->count_all_results();
    }
    
}