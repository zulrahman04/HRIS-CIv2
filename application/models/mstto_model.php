<?php
class Mstto_model extends MY_Model {
    public function getTo(){
        return $this->db->select('*')->from('mst_timeoff')->get()->result();
    }

    public function saveAddTo($code,$ket){

        $this->db->trans_begin();
        $data = array(  'code' => $code,
                        'description' => $ket
                    );
        $this->db->insert('mst_timeoff', $data);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

    public function getToEdit($id){
        return $this->db->select('*')->from('mst_timeoff')->where('id', $id)->get()->row();
    }

    public function saveEditTo($id,$code,$ket){

        $this->db->trans_begin();

        $data = array(  'code' => $code,
                        'description' => $ket
                    );
        $this->db->where('id', $id);
        $this->db->update('mst_timeoff', $data);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

    public function deleteTo($id){

        $this->db->trans_begin();

        $this->db->where('id', $id);
        $this->db->delete('mst_timeoff');

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }
}