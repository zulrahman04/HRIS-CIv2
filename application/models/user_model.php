<?php
class User_model extends MY_Model
{

	public function getListUser() {
        $this->db->select('*');
		$this->db->from('user_hris')->where("NAME != 'developer'");

        $query = $this->db->get();

        //return $this->db->last_query();
        return $query->result();
    }

    public function getListPegawai() {        
        return $this->db->query("SELECT id_pegawai,nama_jabatan,nama FROM pegawai 
        INNER JOIN jabatan
        ON (pegawai.id_jabatan = jabatan.id_jabatan)")->result();
    }

    public function getRole(){
        return $this->db->select('*')->from('rol_mstr')->where_not_in('rol_code', 'DEV')->get()->result();
    }

    public function cekUser($username){
        return $this->db->select('*')->from('user_hris')->where('username', $username)->get()->row();       
    }

    public function cekLevel($id){
        return $this->db->select('*')->from('user_hris')->where('id_pegawai', $id)->get()->row();       
    }

    public function getMyAccountData($usrid,$pass) {
        $this->db->select('id, username, password, name');
        $this->db->from('user_hris');
        $this->db->where('id', $usrid);
        if($pass != ''){
            $this->db->where('password', md5($pass));
        }

        $query = $this->db->get();

        return $query->row();
    }

    public function changePassword($new) {
        $this->db->trans_begin();

        $data = array ( 'password' => md5($new) );
        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('user_hris', $data);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

	public function addUser($id,$username,$role,$status) {

        $pegawai = $this->db->query("SELECT jabatan.nama_jabatan, pegawai.nama 
                                    FROM pegawai INNER JOIN jabatan ON (pegawai.id_jabatan = jabatan.id_jabatan)where id_pegawai = '$id'")->row_array();
        $this->db->trans_begin();

        $data = array (	'id_pegawai' => $id,
                        'username' => $username,
						'password' => md5('123456'),
                        'name' => $pegawai['nama'],
                        'level' => $pegawai['nama_jabatan'],
                        'role' => $role,
                        'status' => $status,
                        'created_by' => $this->session->userdata('username'),
                        'updated_by' => $this->session->userdata('username')
                        );
        $this->db->insert('user_hris', $data);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

    public function editUser($id,$role,$status) {

        $user = $this->session->userdata('username');
        $this->db->trans_begin();

        $this->db->query("UPDATE user_hris SET role = '$role', updated_by = '$user', status = '$status' WHERE id = '$id';");

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

	public function deleteUser($id) {
        $this->db->trans_begin();

        $this->db->where('id', $id);
        $this->db->delete('user_hris');

		// print_r($this->db->last_query()); die();

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
            return false;
		} else {
			$this->db->trans_commit();
            return true;
		}
    }

}
