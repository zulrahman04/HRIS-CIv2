<?php
class Pegawai extends MY_Controller
{
    
	public $layout = 'layout';
	var $API ="";

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('pegawai_model', 'pegawai');
		// $this->load->model('itmbrws_model', 'itmbrws');
    }

    public function index(){
		$this->data['pagetitle'] = 'Pegawai';
        $this->data['menuname'] = 'Administrasi';
        $this->data['submenuname'] = 'Pegawai';
        $this->data['page'] = 'pegawai';

        $this->load->view($this->layout, $this->data);

    }

    public function listPegawai(){
        $list = $this->pegawai->get_datatables();
		$data = array();
		$no = $_POST['start'] + 1;
		foreach ($list as $li) {
			$row = array();

            $row[] = $no++;
			$row[] = $li->nip;
			$row[] = $li->nama;
			$row[] = $li->jk;
			$row[] = $li->nama_departemen;
			$row[] = $li->nama_seksi;
			$row[] = '  <a href="" class="btn btn-info btn-sm"><i class="fa fa-fw fa-eye"></i></a>
                        <a href="" class="btn btn-primary btn-sm"><i class="fa fa-fw fa-edit"></i></a>
                        <a href="" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></a>';
			$data[] = $row;
		}

		$output = array("draw" => $_POST['draw'],
						"recordsTotal" => $this->pegawai->count_all(),
						"recordsFiltered" => $this->pegawai->count_filtered(),
						"data" => $data);
		echo json_encode($output);
    }

	public function add(){
		
        $CI = &get_instance();
        $this->db_FP = $CI->load->database('db2', TRUE);
		$tgl = '2022-10-11';
        $sql = "SELECT name, gender, ssn, user.`USERID`, CHECKTIME, CHECKTYPE
                FROM USERINFO user
                INNER JOIN CHECKINOUT ON user.USERID = CHECKINOUT.USERID 
                WHERE FORMAT(CHECKTIME, 'yyyy-mm-dd') >= '$tgl'
                AND WorkCode = '0'
                order by CHECKTIME asc";
				$query = $this->db_FP->query($sql);
				
        var_dump($query->result());
	}
}