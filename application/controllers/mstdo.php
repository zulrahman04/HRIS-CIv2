<?php
class Mstdo extends MY_Controller
{
    
	public $layout = 'layout';

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('Mstdo_model', 'do');
    }

    public function index()
    {
		$this->data['pagetitle'] = "Day Off";
        $this->data['menuname'] = "Master";
        $this->data['submenuname'] = "Day Off";
        $this->data['page'] = "mstdo";
		
		$this->data['do'] = $this->do->getDo();

        $this->load->view($this->layout, $this->data);
    }

    public function addDo(){
        $tgl = $this->input->post('tgl');
        $ket = $this->input->post('ket');
		$responce = new StdClass;
        if (!$this->do->cekDo($tgl)) {
            if($this->do->saveAddDo($tgl,$ket)){
                $responce->result = 'Berhasil';
                $responce->message = 'Berhasil membuat Day Off';
                echo json_encode($responce);
            }else{
                $responce->result = 'Gagal';
                $responce->message = 'Gagal membuat Day Off';
                echo json_encode($responce);
            }
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Day Off sudah ada';
            echo json_encode($responce);
        }
    }

    public function getDoEdit(){
        $tgl = $this->input->post('tgl');
		echo json_encode($this->do->cekDo($tgl));
    }

    public function saveEditDo(){
        $id = $this->input->post('id');
        $tgl = $this->input->post('tgl');
        $ket = $this->input->post('ket');
		$responce = new StdClass;
        if (!$this->do->cekDo($tgl)) {
            if($this->do->saveEditDo($id,$tgl,$ket)){
                $responce->result = 'Berhasil';
                $responce->message = 'Berhasil membuat Day Off';
                echo json_encode($responce);
            }else{
                $responce->result = 'Gagal';
                $responce->message = 'Gagal membuat Day Off';
                echo json_encode($responce);
            }
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Day Off sudah ada';
            echo json_encode($responce);
        }
    }

    public function deleteDo(){
        $id = $this->input->post('id');
        $responce = new StdClass;

        if($this->do->deleteDo($id)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil menghapus Day Off';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal menghapus Day Off';
            echo json_encode($responce);
        }
    }
}