<?php
class Mstshift extends MY_Controller
{
    
	public $layout = 'layout';
	var $API ="";

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('Mstshift_model', 'shift');
    }

    public function index()
    {
		$this->data['pagetitle'] = "Shift";
        $this->data['menuname'] = "Master";
        $this->data['submenuname'] = "Shift";
        $this->data['page'] = "mstshift";
		
		$this->data['shift'] = $this->shift->getShift();

        $this->load->view($this->layout, $this->data);
    }

    public function addShift(){
        $code = $this->input->post('code');
        $in = $this->input->post('in');
        $out = $this->input->post('out');
		$responce = new StdClass;

        if($this->shift->saveAddShift($code,$in,$out)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil membuat shift';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal membuat shift';
            echo json_encode($responce);
        }
    }

    public function getShiftEdit(){
        $id = $this->input->post('id');
		echo json_encode($this->shift->getShiftEdit($id));
    }

    public function saveEditShift(){
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        $in = $this->input->post('in');
        $out = $this->input->post('out');

		$responce = new StdClass;

        if($this->shift->saveEditShift($id,$code,$in,$out)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Update shift';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Update shift';
            echo json_encode($responce);
        }
    }

    public function deleteShift(){
        $id = $this->input->post('id');
        $responce = new StdClass;

        if($this->shift->deleteShift($id)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil menghapus Shift';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal menghapus Shift';
            echo json_encode($responce);
        }
    }
}