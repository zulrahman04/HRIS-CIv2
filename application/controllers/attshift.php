<?php
class Attshift extends MY_Controller
{
	public $layout = 'layout';
	var $API ="";

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('Attshift_model', 'attshift');
    }

    public function index()
    {
		$this->data['pagetitle'] = 'Generate Shift';
        $this->data['menuname'] = 'Employe Attendance';
        $this->data['submenuname'] = 'Generate Shift';
        $this->data['page'] = 'attshift';
        
        $this->data['shift'] = $this->attshift->getShift();
        $this->data['dept'] = $this->attshift->getDept();

        $this->load->view($this->layout, $this->data);
    }

    public function generate()
    {        
        $dep = $this->input->post('dep');
        $opt1 = $this->input->post('opt1');
        $opt2 = $this->input->post('opt2');
        $opt3 = $this->input->post('opt3');
        $opt4 = $this->input->post('opt4');
        $seksi = $this->input->post('seksi');
        
        $dt = explode("/",$this->input->post('date'));

        $bln = $dt[0];
        $thn = $dt[1];
        $date = strtotime($thn.'-'.$bln.'-01');
        $fdate = date('Y-m-d',$date);
        $ldate = date("Y-m-d", strtotime("+1 month", $date));

		$responce = new StdClass;

        $cek = $this->attshift->cekGenerate($dep,$bln,$thn);
        if (!$cek) {
            $responce->result = 'Berhasil';
            $responce->message = 'Shift Telah Tergenerate Semua';
            echo json_encode($responce);            
        }else{
            $add = $this->attshift->generateAttShift($dep,$fdate,$ldate,$opt1,$opt2,$opt3,$opt4,$seksi);
            if($add){
                $responce->result = 'Berhasil';
                $responce->message = 'Berhasil Generate Shift';
                echo json_encode($responce);
            }else{
                $responce->result = 'Gagal';
                $responce->message = 'Gagal Generate Shift';
                echo json_encode($responce);
            }
        }
    }

    public function getSeksi(){
        $id = $this->input->post('dep');
		echo json_encode($this->attshift->getShiftSelect($id));
    }

    public function listPegawai(){
        $id_dep = $this->input->post('id_dep');
        $id_seksi = $this->input->post('id_seksi');
        $list = $this->attshift->get_datatables($id_dep, $id_seksi);
		$data = array();
		$no = $_POST['start'] + 1;
		foreach ($list as $li) {
			$row = array();

            $row[] = $no++;
			$row[] = $li->nip;
			$row[] = $li->nama;
			$row[] = $li->jk;
			$row[] = $li->nama_departemen;
			$row[] = $li->nama_seksi;
			$data[] = $row;
		}

		$output = array("draw" => $_POST['draw'],
						"recordsTotal" => $this->attshift->count_all($id_dep, $id_seksi),
						"recordsFiltered" => $this->attshift->count_filtered($id_dep, $id_seksi),
						"data" => $data);
		echo json_encode($output);
    }
}