<?php
class Attfp extends MY_Controller
{
    
	public $layout = 'layout';
	var $API ="";

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('Attfp_model', 'attfp');
    }

    public function index()
    {
		$this->data['pagetitle'] = 'Download FP';
        $this->data['menuname'] = 'Employe Attendance';
        $this->data['submenuname'] = 'Download FP';
        $this->data['page'] = 'attfp';
     
        $this->data['data'] = $this->attfp->getFingerPrint();

        $this->load->view($this->layout, $this->data);
    }

    public function import(){

		$responce = new StdClass;

        if($this->attfp->import()){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Import Attendance';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Import Attendance';
            echo json_encode($responce);
        }
    }
}
