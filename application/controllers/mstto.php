<?php
class Mstto extends MY_Controller
{
    
	public $layout = 'layout';

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('Mstto_model', 'to');
    }

    public function index()
    {
		$this->data['pagetitle'] = "Time Off";
        $this->data['menuname'] = "Master";
        $this->data['submenuname'] = "Time Off";
        $this->data['page'] = "mstto";
		
		$this->data['to'] = $this->to->getTo();

        $this->load->view($this->layout, $this->data);
    }

    public function addTo(){
        $code = $this->input->post('code');
        $ket = $this->input->post('ket');
		$responce = new StdClass;

        if($this->to->saveAddTo($code,$ket)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil membuat Time Off';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal membuat Time Off';
            echo json_encode($responce);
        }
    }

    public function getToEdit(){
        $id = $this->input->post('id');
		echo json_encode($this->to->getToEdit($id));
    }

    public function saveEditTo(){
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        $ket = $this->input->post('ket');

		$responce = new StdClass;

        if($this->to->saveEditTo($id,$code,$ket)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Update Time Off';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Update Time Off';
            echo json_encode($responce);
        }
    }

    public function deleteTo(){
        $id = $this->input->post('id');
        $responce = new StdClass;

        if($this->to->deleteTo($id)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil menghapus Time Off';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal menghapus Time Off';
            echo json_encode($responce);
        }
    }
}