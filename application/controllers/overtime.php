<?php
class Overtime extends MY_Controller
{
    
	public $layout = 'layout';

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('Overtime_model', 'overtime');
		// $this->load->model('itmbrws_model', 'itmbrws');
    }

    public function index()
    {
        
		$this->data['pagetitle'] = 'SP Kerja Borongan';
        $this->data['menuname'] = 'Formulir';
        $this->data['submenuname'] = 'SP Kerja Borongan';
        $this->data['page'] = 'overtime';

        $this->load->view($this->layout, $this->data);
    }

    public function listSPKB(){
        $list = $this->overtime->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $li) {
			$row = array();

            $row[] = $li->no_spl;
			$row[] = $li->nama;
			$row[] = $li->tgl_pengajuan;
			$row[] = $li->nama_departemen;
			$row[] = $li->status_approval_form;
			$row[] = '  <a href="" class="btn btn-info btn-sm"><i class="fa fa-fw fa-eye"></i></a>
                        <a href="" class="btn btn-primary btn-sm"><i class="fa fa-fw fa-edit"></i></a>
                        <a href="" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></a>';
			$data[] = $row;
		}

		$output = array("draw" => $_POST['draw'],
						"recordsTotal" => $this->overtime->count_all(),
						"recordsFiltered" => $this->overtime->count_filtered(),
						"data" => $data);
		echo json_encode($output);
    }

    public function create_id($tgl){
        $t_spl = date("Ymd", strtotime($tgl));

        $fcek = 'SPKB-'.$t_spl.'-';
        $cek = $fcek.'00001';

        $cdb = $this->db->where('no_spl', $cek)->get('spl_header');
        $rdb = $cdb->num_rows();

        if($rdb == ''){
            return $cek;
        }else{
            $sdb = $this->db->like('no_spl', $fcek)->order_by('no_spl', 'DESC')->get('spl_header')->row();

            return $fcek.str_pad(substr($sdb->no_spl, -5)+1, 5, '0', STR_PAD_LEFT);
        }
    }
}