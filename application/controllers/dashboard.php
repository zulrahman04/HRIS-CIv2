<?php
class Dashboard extends MY_Controller
{
    
	public $layout = 'layout';

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('Dashboard_model', 'dashboard');
    }

    public function index()
    {
		$this->data['pagetitle'] = $this->session->userdata('dashboardname');
        $this->data['menuname'] = $this->session->userdata('dashboardname');
        $this->data['submenuname'] = '';
        $this->data['page'] = $this->session->userdata('dashboarduri');
     
        // $this->data['data'] = $this->dashboard->getFingerPrint();
        // $this->data['data'] = json_decode($this->curl->simple_get($this->API.'index_get'));

        $this->load->view($this->layout, $this->data);
    }

}
