<?php
class Attendance extends MY_Controller
{
    
	public $layout = 'layout';
	var $API ="";

    public function __construct()
    {
        parent::__construct();
        is_logged_in(); 
        $this->load->model('attendance_model', 'attendance');
		// $this->load->model('itmbrws_model', 'itmbrws');
    }

    public function index()
    {
        $date = $this->input->post('date');
        $dep = $this->input->post('dep');
        $seksi = $this->input->post('seksi');
        if ($date == null) {
            $date = date("Y-m-d");
            $this->data['date'] = date("d/m/Y", strtotime(str_replace('-', '/', $date)));
        }else{
            $date = date("Y-m-d", strtotime(str_replace('/', '-', $date)));
            $this->data['date'] = date("d/m/Y", strtotime(str_replace('-', '/', $date)));
        }
        if ($dep == null) {
            $dep = null;
            $this->data['depart'] = '';
        }else{
            $this->data['depart'] = $this->input->post('dep');
        }
        if ($seksi == null) {
            $seksi = null;
            $this->data['seksi'] = '';
        }else{
            $this->data['seksi'] = $this->input->post('seksi');
        }
		$this->data['pagetitle'] = 'Attendance';
        $this->data['menuname'] = 'Employe Attendance';
        $this->data['submenuname'] = 'Attendance';
        $this->data['page'] = 'attendance';
        
        $this->data['seksiall'] = $this->attendance->getSeksi($dep);
        $this->data['depall'] = $this->attendance->getDep();
        $this->data['code'] = $this->attendance->getCodeAtt();
        $this->data['shift'] = $this->attendance->getShift();
        $this->data['data'] = $this->attendance->getdata($date, $dep, $seksi);

        $this->load->view($this->layout, $this->data);
    }

    public function import(){

		$responce = new StdClass;

        if($this->attendance->import()){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Import attendance';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Import attendance';
            echo json_encode($responce);
        }
    }

    public function getAtt(){

        $id = $this->input->post('id');
        echo json_encode($this->attendance->getAtt($id));
    }

    public function getShift(){
        echo json_encode($this->attendance->getShift());
    }    
    
    public function getCodeAtt(){
        echo json_encode($this->attendance->getCodeAtt());
    }

    public function updAtt(){

		$responce = new StdClass;

        $id = $this->input->post('id');
        $shift = $this->input->post('shift');
        $code = $this->input->post('code');
        $in = $this->input->post('in');
        $out = $this->input->post('out');

        if($this->attendance->updAtt($id, $shift, $code, $in, $out)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Import attendance';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Import attendance';
            echo json_encode($responce);
        }
    }

    public function chgshift(){

        $id = $this->input->post('id');
        $shift = $this->input->post('shift');
		$responce = new StdClass;

        if($this->attendance->chgshift($id, $shift)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Update Shift';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Update Shift';
            echo json_encode($responce);
        }
    }
    public function chgcode(){

        $id = $this->input->post('id');
        $code = $this->input->post('code');
		$responce = new StdClass;

        if($this->attendance->chgcode($id, $code)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Update Code';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Update Code';
            echo json_encode($responce);
        }
    }
    public function chgmsk(){

        $id = $this->input->post('id');
        $masuk = $this->input->post('masuk');
		$responce = new StdClass;

        if($this->attendance->chgmsk($id, $masuk)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Update IN';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Update IN';
            echo json_encode($responce);
        }
    }
    public function chgplg(){

        $id = $this->input->post('id');
        $plg = $this->input->post('plg');
		$responce = new StdClass;

        if($this->attendance->chgplg($id, $plg)){
            $responce->result = 'Berhasil';
            $responce->message = 'Berhasil Update OUT';
            echo json_encode($responce);
        }else{
            $responce->result = 'Gagal';
            $responce->message = 'Gagal Update OUT';
            echo json_encode($responce);
        }
    }
}
